export function run(promises) {
    const resolvedValues = []
    return Promise.all(promises.map(promise => {
        return promise.then(value => resolvedValues.push(value))
    })).then(() => resolvedValues)
}
