import { run } from "./index";

const resolveAfter = (time, value) =>
  new Promise(resolve => setTimeout(() => resolve(value), time));

test("run with empty arguments", done => {
  run([]).then(result => {
    expect(result).toEqual([]);
    done();
  });
});

test("run with one promise", done => {
  const p = Promise.resolve(1);

  run([p]).then(result => {
    expect(result).toEqual([1]);
    done();
  });
});

test("run with two promises that resolve in order they were placed", done => {
  const p1 = resolveAfter(0, 1);
  const p2 = resolveAfter(200, 2);

  run([p1, p2]).then(result => {
    expect(result).toEqual([1, 2]);
    done();
  });
});

test("run with two promises that resolve in revert order they were placed", done => {
  const p1 = resolveAfter(0, 1);
  const p2 = resolveAfter(200, 2);

  run([p2, p1]).then(result => {
    expect(result).toEqual([1, 2]);
    done();
  });
});

test("run with three promises that resolve in order", done => {
  const p1 = resolveAfter(0, 1);
  const p2 = resolveAfter(200, 2);
  const p3 = resolveAfter(500, 3);

  run([p1, p2, p3]).then(result => {
    expect(result).toEqual([1, 2, 3]);
    done();
  });
});

test("run with three promises that resolve in revert order", done => {
  const p1 = resolveAfter(0, 1);
  const p2 = resolveAfter(200, 2);
  const p3 = resolveAfter(500, 3);

  run([p3, p2, p1]).then(result => {
    expect(result).toEqual([1, 2, 3]);
    done();
  });
});

test("run with three promises that second is resolved first, first is resolved last, and last is resolved as second", done => {
  const p1 = resolveAfter(0, 1);
  const p2 = resolveAfter(200, 2);
  const p3 = resolveAfter(500, 3);

  run([p3, p1, p2]).then(result => {
    expect(result).toEqual([1, 2, 3]);
    done();
  });
});
